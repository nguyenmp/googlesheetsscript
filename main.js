/* eslint max-statements: 0 */

//naming conventions as follows:
//snakecase for variables! <==> NEVER! This is JavaScript, not Python
// <==> Cell class handles this more appropriately
//rc for row/column of a cell - first_name_rc = [row, column] of first_name cell
//r for row - channel_header_r = channel header row
//c for column - mattermost_c = mattermost column
// <==> NEVER! cellTextCell is more apt
//CAPS for name of cells - first name, last name, student info, etc

// easier to change here than dig into code and modify
var TESTING = true;
var INSTRUCTOR_EMAIL = 'jfranz@codingdojo.com';
var CM_EMAIL = 'jcapo@codingdojo.com';
// for emails ....
var CM_NAME = 'Jessica';
var INSTRUCTOR_NAME = 'Jason';

// DO NOT EDIT BELOW THIS LINE (unless you know what you're doing...)

var STATUS_EMAIL_LOOKUP = {
  'PROBATION (AP)': 'PROBATION',
  'FALLING BEHIND (B)': 'FALLING_BEHIND',
  'DROPPED (D)': 'WITHDRAW',
  'ROLL BACK (RB)': 'ROLL BACK',
  NEXT_STACK: 'NEXT STACK',
};

var SCRIPT_TYPES_ENUM = {
  DT_SCRIPT: 'Discussion Topic',
  RETRIEVE_DT_SCRIPT: 'Retrieve Discussion Topics',
  ATTENDANCE_SCRIPT: 'Attendance',
  ASSIGNMENT_SCRIPT: 'Assignment',
  DT_DUE_DATES: 'DT Due Dates',
};

var STATUS_ENUM = {
  DROPPED: 'DROPPED (D)',
  ROLL_BACK: 'ROLL BACK (RB)',
  PAUSED: 'PAUSED (P)',
  POSTPONED: 'POSTPONED (PPD)',
  SELF_PACED: 'SELF PACED (SP)',
  UNKNOWN: 'UNKNOWN (U)',
  EXPELLED: 'EXPELLED (E)',
  ON_PACE: 'ON PACE (OP)',
  PROBATION: 'PROBATION (AP)',
  FALLING_BEHIND: 'FALLING BEHIND (B)',
};

var URLS = {
  base: 'http://minhmax.com/',
  assignments: 'http://minhmax.com/getAssignments',
  topicParticipants: 'http://minhmax.com/getDiscussionNames',
  search: 'https://dojo.news/api/v4/users/search',
  login: 'https://dojo.news/api/v4/users/login',
};

var DISPLAY_VALUES = {
  RUN: 'RUN',
  CLEAR: 'CLEAR',
  STOP: 'STOP',
  EMAIL: 'EMAIL',
  NOID: 'Cannot find Id',
  STATUS: 'STATUS',
  REQUESTED: 'REQUESTED',
  NEXT_STACK: 'NEXT STACK',
};

var HEADER = {
  MATTERMOST: 'MATTERMOST',
  STUDENT_NAME: 'STUDENT NAME',
  NEXT_STACK: 'NEXT STACK',
  STUDENT_INFO: 'STUDENT INFO',
  EMAIL: 'EMAIL',
  STATUS: 'STATUS',
  SCRIPTS: 'SCRIPTS SECTION',
  DISCUSSION: 'DISCUSSION',
  LATEST: 'LATEST ASSIGNMENT',
  SCRIPT_TYPE: 'Script Type',
  WEEK: 'WEEK ',
  QUESTION: 'QUESTION ',
  DT_COLUMN: 'DT Column',
};

var START_ROW_OF_DATA = 5;
var ut = Utilities;

// =======================================================================
// POLYFILLS
if (typeof Object.create !== 'function') {
  Object.create = function(proto, propertiesObject) {
    if (typeof proto !== 'object' && typeof proto !== 'function') {
      throw new TypeError('Object prototype may only be an Object: ' + proto);
    } else if (proto === null) {
      throw new Error(
        "This browsers implementation of Object.create is a shim and does not support 'null' as the first argument."
      );
    }

    if (typeof propertiesObject != 'undefined') {
      throw new Error(
        "This browser's implementation of Object.create is a shim and doesn't support a second argument."
      );
    }

    function F() {}
    F.prototype = proto;

    return new F();
  };
}
// eslint-disable-next-line no-extend-native
String.prototype.supplant = function(o) {
  return this.replace(/{([^{}]*)}/g, function(a, b) {
    var r = o[b];
    return isStringOrNumber(r) ? r : a;
  });
};

// ========================== CELL CLASSES =============================================

var Cell = (function() {
  function Cell(text, row, column) {
    this.text = text;
    this.row = row;
    this.column = column;
  }

  Cell.from = function(text, values, startData) {
    startData = startData || [1, 1];
    var row = startData[0];
    var column = startData[1];

    for (var rowId = row - 1; rowId < values.length; rowId++) {
      for (
        var columnId = column - 1;
        columnId < values[rowId].length;
        columnId++
      ) {
        if (values[rowId][columnId] === text) {
          return new Cell(text, rowId + 1, columnId + 1);
        }
      }
    }

    throw new Error("Unable to locate cell with text '" + text + "'");
  };
  return Cell;
})();

var StudentCell = (function() {
  function StudentCell(values) {
    this.cell = Cell.from(HEADER.STUDENT_NAME, values);
  }

  Object.defineProperties(StudentCell.prototype, {
    firstName: {
      get: function() {
        return this.cell.column;
      },
    },
    lastName: {
      get: function() {
        return this.firstName + 1;
      },
    },
  });

  return StudentCell;
})();

var ActiveCell = (function() {
  function ActiveCell(cell) {
    this.cell = cell;
  }

  ActiveCell.prototype.setValue = function(value) {
    this.cell.setValue(value);
  };
  Object.defineProperties(ActiveCell.prototype, {
    row: {
      get: function() {
        return this.cell.getRow();
      },
    },
    column: {
      get: function() {
        return this.cell.getColumn();
      },
    },
    displayValue: {
      get: function() {
        return this.cell.getDisplayValue();
      },
    },
  });

  return ActiveCell;
})();

/* ========================= EMAIL CLASSES =========================== */

var Email = (function() {
  function Email(sendTo, subject, values) {
    this.to = TESTING ? INSTRUCTOR_EMAIL : sendTo;
    this.subject = subject;
    // temporary
    this.htmlBody = renderEmailTemplate(values);
  }

  Email.from = function(reason, values) {
    try {
      // eslint-disable-next-line no-use-before-define
      return new EMAILS[reason](values);
    } catch (e) {
      throw new Error(reason + ' is not a valid emailer at this time!');
    }
  };

  Email.prototype.send = function() {
    this.mailer.sendEmail(this);
  };

  Email.mailer = MailApp;

  Object.defineProperty(Email.prototype, 'mailer', {
    get: function() {
      return Email.mailer;
    },
  });

  return Email;
})();

var EmailRollback = (function(Email) {
  function EmailRollback(values) {
    var subject =
      'Roll Back - ' +
      values.first_name +
      ' (' +
      values.student_email_address +
      ')' +
      ' to ' +
      values.rollback.class_type +
      ' ' +
      values.rollback.date;
    Email.call(this, CM_EMAIL, subject, values);
  }

  EmailRollback.prototype = Object.create(Email.prototype);
  EmailRollback.prototype.constructor = EmailRollback;

  return EmailRollback;
})(Email);

var EmailFallingBehind = (function(Email) {
  function EmailFallingBehind(values) {
    var subject = 'Assignment status update?';
    Email.call(this, values.student_email_address, subject, values);
  }

  EmailFallingBehind.prototype = Object.create(Email.prototype);
  EmailFallingBehind.prototype.constructor = EmailFallingBehind;

  return EmailFallingBehind;
})(Email);

var EmailWithdraw = (function(Email) {
  function EmailWithdraw(values) {
    var subject =
      'Withdraw - ' +
      values.first_name +
      '(' +
      values.student_email_address +
      ')';
    Email.call(this, CM_EMAIL, subject, values);
  }

  EmailWithdraw.prototype = Object.create(Email.prototype);
  EmailWithdraw.prototype.constructor = EmailWithdraw;

  return EmailWithdraw;
})(Email);

var EmailNextStack = (function(Email) {
  function EmailNextStack(values) {
    var subject = 'Next stack choice?';
    Email.call(this, values.student_email_address, subject, values);
  }

  EmailNextStack.prototype = Object.create(Email.prototype);
  EmailNextStack.prototype.constructor = EmailNextStack;

  return EmailNextStack;
})(Email);

var EmailProbation = (function(Email) {
  function EmailProbation(values) {
    var subject = 'Status update and attendance warning';
    Email.call(this, values.student_email_address, subject, values);
  }

  EmailProbation.prototype = Object.create(Email.prototype);
  EmailProbation.prototype.constructor = EmailProbation;

  return EmailProbation;
})(Email);

var EMAIL_TYPES_ENUM = {
  PROBATION: 'PROBATION',
  FALLING_BEHIND: 'FALLING_BEHIND',
  WITHDRAW: 'WITHDRAW',
  ROLL_BACK: 'ROLL BACK',
  NEXT_STACK: 'NEXT STACK',
};

var EMAILS = {
  'ROLL BACK': EmailRollback,
  FALLING_BEHIND: EmailFallingBehind,
  'NEXT STACK': EmailNextStack,
  WITHDRAW: EmailWithdraw,
  PROBATION: EmailProbation,
};

function sendEmails(data) {
  Email.from(data.email_type, data).send();
}

/* ========================= SHEET CLASS =========================== */

var Sheet = (function() {
  function fromArgs(args, start) {
    return Array.prototype.slice.call(args, start || 0);
  }

  function Sheet() {
    this.sheet = SpreadsheetApp.getActiveSheet();
    this.ut = Utilities;
    this.ui = SpreadsheetApp.getUi();
    this.values = this.sheet.getDataRange().getValues();
    this.activeCell = new ActiveCell(this.sheet.getActiveCell());

    // hopefully temporary...
    this.headerRow = Cell.from(HEADER.STUDENT_INFO, this.values).row + 1;
  }

  Sheet.logger = Logger;
  Sheet.log = function(content) {
    this.logger.log(content);
  };

  Sheet.prototype.toast = function(title, message, timeout) {
    this.spreadsheet.toast(title, message, timeout || 2);
  };

  Sheet.prototype.formatDate = function(date) {
    return this.ut.formatDate(
      new Date(date || Date.now()),
      'PTZ',
      'dd-MM-yyyy'
    );
  };

  Sheet.prototype.range = function() {
    return this.sheet.getRange.apply(this.sheet, fromArgs(arguments));
  };

  Sheet.prototype.displayValue = function() {
    return this.range.apply(this, fromArgs(arguments)).getDisplayValue();
  };

  Sheet.prototype.displayValues = function() {
    return this.range.apply(this, fromArgs(arguments)).getDisplayValues();
  };

  Sheet.prototype.setValue = function(value) {
    this.range.apply(this, fromArgs(arguments, 1)).setValue(value);
  };

  Sheet.prototype.valueAt = function() {
    return this.range.apply(this, fromArgs(arguments)).getValue();
  };

  Sheet.prototype.log = function(content) {
    Sheet.log(content);
  };

  Object.defineProperties(Sheet.prototype, {
    activeRow: {
      get: function() {
        return this.activeCell.row;
      },
    },

    activeColumn: {
      get: function() {
        return this.activeCell.column;
      },
    },

    activeValue: {
      get: function() {
        return this.activeCell.displayValue;
      },
    },

    spreadsheet: {
      get: function() {
        return SpreadsheetApp.getActiveSpreadsheet();
      },
    },
  });

  return Sheet;
})();

var Property = {
  service: PropertiesService,
  from: function(property) {
    return this.service.getScriptProperties().getProperty(property);
  },
};

function myOnEdit() {
  logEmailQuota();
  var sheet = new Sheet();

  sheet.toast('Log', 'On Edit works');
  var ui = SpreadsheetApp.getUi();
  var data = sheet.values;
  var studentNameCell = new StudentCell(sheet.values);
  //REQUIRED COLUMNS
  var scriptCell = Cell.from(HEADER.SCRIPTS, sheet.values);
  var emailCell = Cell.from(HEADER.EMAIL, sheet.values);
  var discussionCell = Cell.from(HEADER.DISCUSSION, sheet.values);
  var statusCell = Cell.from(HEADER.STATUS, sheet.values);

  // temporary

  try {
    var latestAssignmentCell = '';
    var nextStackCell = '';
    latestAssignmentCell = Cell.from(HEADER.LATEST, sheet.values);
    nextStackCell = Cell.from(HEADER.NEXT_STACK, sheet.values);
  } catch (e) {
    // do nothing, not pyII or older sheet
  }

  //REQUIRED ROWS

  // part of sheet instance now, access via sheet.headerRow
  // var columnHeadersRow = studentCell.row + 1;

  //GETS MATTERMOST STUFF
  if (isMattermost(sheet)) {
    return handleMattermost(sheet);
  }

  //DISCUSSION TOPICS/SCRIPTS
  if (sheet.activeValue === DISPLAY_VALUES.RUN) {
    // var scriptTypeCell = Cell.from(HEADER.SCRIPT_TYPE, sheet.values);
    var scriptData = sheet.displayValues(
      scriptCell.row,
      scriptCell.column,
      4,
      4
    );
    var scriptType = scriptData[3][0];
    switch (scriptType) {
      case SCRIPT_TYPES_ENUM.RETRIEVE_DT_SCRIPT:
        updateDiscussionTopic(sheet);
        break;
      case SCRIPT_TYPES_ENUM.ASSIGNMENT_SCRIPT:
        var emails = [];
        var rowCount = START_ROW_OF_DATA;
        var requiredStatuses = [
          STATUS_ENUM.ON_PACE,
          STATUS_ENUM.PROBATION,
          STATUS_ENUM.FALLING_BEHIND,
        ]; //only check on these statuses

        var status = sheet.displayValue(rowCount, statusCell.column);
        while (
          sheet.displayValue(rowCount, emailCell.column).indexOf('@') > 0 &&
          statusIsInRequired(requiredStatuses, status)
        ) {
          emails.push(sheet.displayValue(rowCount, emailCell.column));
          emails.push(
            cleanString(
              sheet.displayValue(rowCount, studentNameCell.firstName) +
                sheet.displayValue(rowCount, studentNameCell.lastName)
            )
          );
          status = sheet.displayValue(++rowCount, statusCell.column);
        }

        Sheet.log(emails);
        var stack = scriptData[3][3];
        var findNameOptions = {
          contentType: 'application/json',
          payload: JSON.stringify({
            'emails[]': emails,
            stack: stack,
          }),
          method: 'post',
        };
        var assignmentData = JSON.parse(
          UrlFetchApp.fetch(URLS.assignments, findNameOptions).getContentText()
        ).data;

        Sheet.log(assignmentData);
        for (var i = 0; i < assignmentData.length; i++) {
          var studentData = assignmentData[i];
          var studentEmail = studentData.email.toLowerCase();
          var current = studentData.current;
          var latestData = studentData.latest;
          for (var k = 0; k < data.length; k++) {
            var listed_student_email = data[k][emailCell.column - 1]; //minus one due to data is index
            if (isNotEmptyString(listed_student_email)) {
              if (
                cleanString(listed_student_email).indexOf(
                  cleanString(studentEmail)
                ) !== -1
              ) {
                //[1] because column B
                var cell = sheet.range(k + 1, emailCell.column);
                cell.setNote(
                  ut.formatString(
                    'latest:%s(%s) \n Current: %s \n num_completed_optionals:%s \n num_completed:%s',
                    current,
                    latestData.name,
                    sheet.formatDate(latestData.completed_date),
                    studentData.num_completed_optionals,
                    studentData.num_completed
                  )
                );
                if (latestAssignmentCell.column) {
                  sheet.setValue(
                    studentData.latest.name,
                    k + 1,
                    latestAssignmentCell.column
                  );
                  sheet
                    .range(k + 1, latestAssignmentCell.column)
                    .setNote(
                      ut.formatString(
                        'latest:%s(%s) \n Current: %s \n num_completed_optionals:%s \n num_completed:%s',
                        latestData.name,
                        sheet.formatDate(latestData.completed_date),
                        current,
                        studentData.num_completed_optionals,
                        studentData.num_completed
                      )
                    );
                }
                break;
              }
            }
          }
        }
    }

    sheet.activeCell.setValue(DISPLAY_VALUES.STOP);
    sheet.toast('Ending', 'Done');
    //STATUS UPDATES
  } else if (sheet.activeValue === DISPLAY_VALUES.CLEAR) {
    var startRange = sheet.range(START_ROW_OF_DATA, scriptCell.column);
    var endRange = sheet.range(START_ROW_OF_DATA, startRange.getColumn() + 1);
    sheet
      .range(
        startRange.getA1Notation() +
          ':' +
          endRange.getA1Notation().replace(/\d+/g, '')
      )
      .clear();
  } else if (
    sheet.displayValue(sheet.headerRow, sheet.activeColumn) ===
    DISPLAY_VALUES.STATUS
  ) {
    var student_email_address = sheet.valueAt(
      sheet.activeRow,
      emailCell.column
    );
    var discussion_totals = sheet.valueAt(
      sheet.activeRow,
      discussionCell.column
    );
    var first_name_cell = sheet.range(
      sheet.activeRow,
      studentNameCell.firstName
    );
    var first_name = first_name_cell.getDisplayValue();
    var status = sheet.activeValue;
    var update;
    var options = {
      student_email_address: student_email_address,
      first_name: first_name,
      email_type: STATUS_EMAIL_LOOKUP[status],
      rollback: {},
      discussion_totals: discussion_totals,
    };
    var sampleEmail = renderEmailTemplate(options).replace(/(?:<br>)/g, '\n');
    options['discussion_totals'] = discussion_totals;

    Sheet.log(status);
    Sheet.log('here');
    Sheet.log(status === STATUS_ENUM.PROBATION);

    switch (status) {
      case STATUS_ENUM.PROBATION:
        Sheet.log('here ye');
        Sheet.log(status);
        var response = sheet.ui.alert(
          'Email?',
          'Would you like to send a status update email?\n Email will say:\n' +
            sampleEmail,
          ui.ButtonSet.YES_NO
        );
        Sheet.log(response === ui.Button.YES);
        if (response === ui.Button.YES) {
          Sheet.log('got to status enum');
          update = ut.formatString(
            '%sChanged status to %s and emailed for update at: %s \n',
            first_name_cell.getNote(),
            status,
            sheet.formatDate()
          );
          sendEmails(options);
        }
        break;
      case STATUS_ENUM.FALLING_BEHIND:
        var response = ui.alert(
          'Email?',
          'Would you like to send a status update email?\n Email will say:\n' +
            sampleEmail,
          ui.ButtonSet.YES_NO
        );
        if (response === ui.Button.YES) {
          update = ut.formatString(
            '%sChanged status to %s and emailed for update at: %s \n',
            first_name_cell.getNote(),
            status,
            sheet.formatDate()
          );
          sendEmails(options);
        }
        break;
      case STATUS_ENUM.DROPPED:
        var response = ui.prompt(
          'Email awesome ' + CM_NAME + '?',
          'Would you like to send email to ' +
            CM_NAME +
            ' with the name and email of student? If so, please state the reason for withdrawal. \n Email will say:\n' +
            sampleEmail,
          ui.ButtonSet.YES_NO
        );
        if (response.getSelectedButton() === ui.Button.YES) {
          update = ut.formatString(
            '%sChanged status to %s(%s) and emailed ' + CM_NAME + ' at: %s \n',
            first_name_cell.getNote(),
            status,
            response.getResponseText(),
            sheet.formatDate()
          );
          options['reason'] = response.getResponseText();
          sendEmails(options);
        }
        break;
      case STATUS_ENUM.ROLL_BACK:
        var response = ui.alert(
          'Would you like to send a email to ' +
            CM_NAME +
            ' with information? \n Email will say:\n' +
            sampleEmail,
          ui.ButtonSet.YES_NO
        );
        if (response === ui.Button.YES) {
          var htmlApp = HtmlService.createHtmlOutput(
            generateHtml(first_name, student_email_address)
          )
            .setTitle('My HtmlService Application')
            .setWidth(250)
            .setHeight(200);
          SpreadsheetApp.getActiveSpreadsheet().show(htmlApp);
          update = ut.formatString(
            '%sChanged status to %s at: %s \n',
            first_name_cell.getNote(),
            status,
            sheet.formatDate()
          );
        } else {
          var response = ui.prompt(
            'Note Info',
            'Please type the note regarding the rollback(ie. "Python 1, April, no fee, etc")',
            ui.ButtonSet.YES_NO
          );
          update = ut.formatString(
            '%sChanged status to %s(%s) at: %s \n',
            first_name_cell.getNote(),
            status,
            response.getResponseText(),
            sheet.formatDate()
          );
        }
        break;
      case STATUS_ENUM.PAUSED:
        var response = ui.prompt(
          'Reason?',
          'Please State reason for pause or postponement',
          ui.ButtonSet.OK
        );
        update = ut.formatString(
          '%sChanged status to %s(%s): %s \n',
          first_name_cell.getNote(),
          status,
          response.getResponseText(),
          sheet.formatDate()
        );
        break;
      default:
        break;
    }
    if (!update) {
      update = ut.formatString(
        '%sChanged status to %s at: %s \n',
        first_name_cell.getNote(),
        status,
        sheet.formatDate()
      );
    }
    first_name_cell.setNote(update);
  } else if (
    nextStackCell.column &&
    sheet
      .displayValue(sheet.headerRow, nextStackCell.column)
      .getDisplayValue() === DISPLAY_VALUES.NEXT_STACK
  ) {
    var student_email_address = sheet.valueAt(
      sheet.activeRow,
      emailCell.column
    );
    var first_name_cell = sheet.range(
      sheet.activeRow,
      studentNameCell.firstName
    );
    var first_name = first_name_cell.getDisplayValue();
    if (sheet.activeValue === DISPLAY_VALUES.REQUESTED) {
      var options = {
        student_email_address: student_email_address,
        first_name: first_name,
        email_type: EMAIL_TYPES_ENUM.NEXT_STACK,
      };
      sendEmails(options);
    }
  }
}

function submitForm(data) {
  var sheet = new Sheet();
  var emailCell = Cell.from(HEADER.EMAIL, sheet.values);
  var studentEmail = sheet.valueAt(sheet.activeRow, emailCell.column);
  var studentNameCell = Cell.from(HEADER.STUDENT_NAME, sheet.values);
  var firstNameCellx = sheet.range(sheet.activeRow, studentNameCell.column);
  var firstName = firstNameCellx.getDisplayValue();
  firstNameCellx.setNote(
    firstNameCellx.getNote() +
      'Changed status to ROLLBACK (' +
      data.class_type +
      ' ' +
      data.date +
      ' - ' +
      data.reason +
      ') at: ' +
      new Date() +
      '\n'
  );
  var options = {
    student_email_address: studentEmail,
    first_name: firstName,
    email_type: EMAIL_TYPES_ENUM.ROLL_BACK,
    rollback: data,
  };
  sendEmails(options);
}

function getUserName(
  sheet,
  userNameColumn,
  userNameRow,
  mattermostColumn,
  token,
  activeCell
) {
  var findNameOptions = {
    contentType: 'application/json',
    payload: JSON.stringify({
      term: sheet.displayValue(userNameRow, userNameColumn),
    }),
    method: 'post',
    headers: {
      Authorization: 'Bearer ' + token,
    },
  };
  if (activeCell) {
    findNameOptions['payload'] = JSON.stringify({
      term: activeCell,
    });
  }
  var students = JSON.parse(
    UrlFetchApp.fetch(URLS.search, findNameOptions).getContentText()
  );
  Logger.log(students);
  if (students.length >= 1) {
    sheet.setValue(students[0].username, userNameRow, mattermostColumn);
  }
}

//data - object with data containing
//{
// to: str <delivering to email>, subject:str <subject of the email>, cc: array<cc any person>, email_type: EMAIL_TYPES_ENUM<type of email being sent>
// student_first_name: str<first name of student>
// rollback: object,
// discussion_totals: <total amount of discussions from spreadsheet
//}
//rollback object -
//{ class_type: str <class to rollback ie. Python 1>, date: str <Month of class>, fee: "FEE" or "NO FEE", reason: str, <reason for no fee, required if fee="NO FEE"> }

function updateDiscussionTopic(sheet) {
  sheet.toast('Starting', 'Wait until script finishes to run something else');

  var studentNameCell = new StudentCell(sheet.values);
  var scriptCell = Cell.from(HEADER.SCRIPTS, sheet.values);
  var scriptTypeCell = Cell.from(HEADER.SCRIPT_TYPE, sheet.values);
  var scriptsSectionColumn = scriptCell.column;
  var scriptData = sheet.displayValues(scriptCell.row, scriptCell.column, 4, 4);

  var startRange = sheet.range(START_ROW_OF_DATA, scriptCell.column);
  var endRange = sheet.range(START_ROW_OF_DATA, startRange.getColumn() + 1);

  sheet
    .range(
      startRange.getA1Notation() +
        ':' +
        endRange.getA1Notation().replace(/\d+/g, '')
    )
    .clear();

  var url = scriptData[3][2];
  var week = url.substring(url.indexOf('(') + 1, url.indexOf('.'));
  var day = url.substring(url.indexOf('.') + 1, url.indexOf(')'));
  url = url.substring(0, url.indexOf('('));

  var weekCell = Cell.from(HEADER.WEEK + week, sheet.values);
  var dayCell = Cell.from(HEADER.QUESTION + day, sheet.values);
  var discussionTopicCell = Cell.from(HEADER.QUESTION + day, sheet.values, [
    dayCell.row,
    weekCell.column,
  ]);
  var discussionTopicColumn = sheet
    .range(discussionTopicCell.row, discussionTopicCell.column)
    .getA1Notation()
    .replace(/\d+/g, '');
  var topicAreaCell = Cell.from(HEADER.DT_COLUMN, sheet.values);
  sheet.setValue(
    discussionTopicColumn,
    topicAreaCell.row + 1,
    topicAreaCell.column
  );
  var date = sheet.displayValue(discussionTopicColumn + 3);

  date = date.split('/');
  date[2] = 20 + date[2];
  date = date.join('/');

  var discussionColumnDataCell = Cell.from(HEADER.DT_COLUMN, sheet.values);
  var findDiscussionTopicsNames = {
    contentType: 'application/json',
    payload: JSON.stringify({
      date: date,
      url: url,
      sheetId: SpreadsheetApp.getActiveSpreadsheet().getId(),
      sheetName: sheet.sheet.getSheetName(),
      cell: sheet
        .range(scriptCell.row + 4, scriptsSectionColumn)
        .getA1Notation(),
    }),
    method: 'post',
  };
  var students = JSON.parse(
    UrlFetchApp.fetch(URLS.topicParticipants, findDiscussionTopicsNames)
  );
  sheet.setValue(
    SCRIPT_TYPES_ENUM.DT_SCRIPT,
    scriptTypeCell.row + 1,
    scriptTypeCell.column
  );
  sheet
    .range(START_ROW_OF_DATA, scriptsSectionColumn, students.data.length)
    .setValues(students.data);

  var discussionTopicColumn =
    scriptData[3][1] ||
    sheet.displayValue(
      discussionColumnDataCell.row + 1,
      discussionColumnDataCell.column
    );

  var start = START_ROW_OF_DATA;
  while (isNotEmpty(sheet.displayValue(start, scriptsSectionColumn))) {
    sheet.setValue(start, scriptCell.row + 1, scriptsSectionColumn + 2);
    var value = sheet.displayValue(start, scriptsSectionColumn);
    var index = value.indexOf(':');

    if (index > 0) {
      sheet.setValue(value.substring(index + 4), start, scriptsSectionColumn);
    }

    var studentName = cleanString(sheet.valueAt(start, scriptsSectionColumn));

    if (isEmpty(studentName)) {
      return;
    }

    var data = sheet.values;
    for (var row = 0; row < data.length; row++) {
      var listed_student_name =
        data[row][studentNameCell.firstName - 1] +
        data[row][studentNameCell.lastName - 1]; //minus one due to data is index
      if (
        isNotEmptyString(listed_student_name) &&
        cleanString(listed_student_name).indexOf(studentName) !== -1
      ) {
        //[1] because column B
        sheet.setValue(row + 1, start, scriptsSectionColumn + 1);
        sheet.log(discussionTopicColumn + (row + 1));
        sheet.setValue('X', discussionTopicColumn + (row + 1));
      }
    }
    start++;
  }
  sheet.setValue('', scriptCell.row + 3, scriptCell.column + 1);
  sheet.setValue(
    SCRIPT_TYPES_ENUM.RETRIEVE_DT_SCRIPT,
    scriptTypeCell.row + 1,
    scriptTypeCell.column
  );
}
//GETS MATTERMOST AUTHENTICATION TOKEN
function getAuthToken() {
  var tokenOptions = {
    contentType: 'application/json',
    payload: JSON.stringify({
      // eslint-disable-next-line camelcase
      login_id: Property.from('MATTERMOSTEMAIL'),
      password: Property.from('MATTERMOSTPASSWORD'),
    }),
    method: 'post',
  };
  var response = UrlFetchApp.fetch(URLS.login, tokenOptions);
  return response.getAllHeaders().token;
}

function statusIsInRequired(requiredStatuses, status) {
  return requiredStatuses.indexOf(status) > -1;
}
//RENDERS TEMPLATE EMAILS
//PARAMETER : data = { email_type:<type of email>, first_name:<student_first_name>, discussion_total:<discussion totals from spreadsheet>, reason: <reason for withdrawal>
//                     student_email_address:<students email address>,
//                     rollback: { class_type: str <class to rollback ie. Python 1>, date: str <Month of class>,
//                                 fee: "FEE" or "NO FEE", reason: str, <reason for no fee, required if fee="NO FEE">
// .                             }
//                    }
function renderEmailTemplate(data) {
  var message = '';
  switch (data.email_type) {
    case EMAIL_TYPES_ENUM.ROLL_BACK:
      message =
        'Hi ' +
        CM_NAME +
        ', <br><br> \
            Please put ' +
        data.first_name +
        '(' +
        data.student_email_address +
        ' in the ' +
        (data.rollback.class_type || '<python 1>') +
        ' for ' +
        (data.rollback.date || '<May>') +
        ' with ' +
        (data.rollback.fee || '<fee or no fee>(reason for no fee)');
      if (data.rollback.fee === 'NO FEE') {
        message += '(' + data.rollback.reason + ')';
      }
      message +=
        ' . Let me know if you need any further information.<br><br>' +
        'Thank you!<br> \
            ' +
        INSTRUCTOR_NAME;
      break;
    case EMAIL_TYPES_ENUM.PROBATION:
      message =
        'Hi ' +
        data.first_name +
        ',<br>\
            <br>\
            How are you doing? I hope this email reaches you well. We just wanted to bring to your attention that you are falling below\
            the needed discussion totals (currently at ' +
              data.discussion_totals +
            '). 80% of your discussion topics are required and failing\
            to meet this mark can be grounds for removal from the course. If you can, please let your instructor\
            know your current status by tomorrow to solve this situation otherwise we may have to withdraw you from the course. Hoping to hear from you!<br>\
            <br>\
            Sincerely,<br>\
            Online Team';
      break;
    case EMAIL_TYPES_ENUM.FALLING_BEHIND:
      message =
        'Hello ' +
        data.first_name +
        ',<br><br> \
            How are you? We havent seen many assignments uploaded to your account\
            and wanted to make sure you are getting support and everything was going okay.\
            Just a reminder that 70% of the non-optional assignments need to be completed for the course. \
            Please let your current instructor know what is going on and if we can help with anything.<br><br>\
            Sincerely,<br>\
            Online Team';
      break;
    case EMAIL_TYPES_ENUM.WITHDRAW:
      message =
        'Hi ' +
        CM_NAME +
        ', <br><br>\
            Please help ' +
        data.first_name +
        '(' +
        data.student_email_address +
        ') with the withdrawal process(' +
        (data.reason || '<REASON>') +
        '). Let me know if you need any further information.<br><br>\
            Thank you!<br> \
            ' +
        INSTRUCTOR_NAME;
      break;
    case EMAIL_TYPES_ENUM.NEXT_STACK:
      message =
        'Hi ' +
        data.first_name +
        ',<br> \
            You have passed the belt but I am unsure of what you want to learn next!\
            Let me know your next stack choice at your earliest convenience.<br><br>\
            Thank you,<br> \
            ' +
        INSTRUCTOR_NAME;
  }
  return message;
}

function generateHtml(name, email) {
  var html =
    '\
    <!DOCTYPE html>\
        <html>\
            <head>\
                <base target="_top">\
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>\
                <script>\
                    function onSuccess() {\
                        google.script.host.close()\
                    };\
                    function sendText(){\
                        var class_type =$("#class").val();\
                        var date =$("#date").val();\
                        var fee = $("#fee").val();\
                        var reason = $("#reason").val();\
                        google.script.run.withSuccessHandler(onSuccess).submitForm({\
                            class_type:class_type,\
                            date:date,\
                            fee:fee,\
                            reason:reason,\
                            name:"` + name + `",\
                            email:" email + "\
                        });\
                    };\
                </script>\
            </head>\
        <body>\
            <p> Do not switch cells until this box closes! Need the status to be active cell when form submits!</p>\
            <select name="class" id="class">\
                <option value="Webfundamentals">Webfun</option>\
                <option value="Python 1">Python 1</option>\
                <option value="Python 2">Python 2</option>\
                <option value="C# 1">C# 1</option>\
                <option value="C# 2">C# 2</option>\
                <option value="MEAN 1">MEAN 1</option>\
                <option value="MEAN 2">MEAN 2</option>\
            </select>\
            <select name="date" id="date">\
                <option value="April">April</option>\
                <option value="May">May</option>\
                <option value="June">June</option>\
                <option value="July">July</option>\
                <option value="August">August</option>\
                <option value="September">September</option>\
                <option value="October">October</option>\
                <option value="November">November</option>\
                <option value="December">December</option>\
            </select>\
            <select name="fee" id="fee">\
                <option value="fee">Fee</option>\
                <option value="NO FEE">No Fee</option>\
            </select>\
            <input type="text" name="reason" id="reason">\
            <button onClick="sendText();">submit</button>\
        </body>\
    </html>';
  return html;
}
function cleanString(str) {
  return str
    .replace(/\s+/g, '')
    .trim()
    .toLowerCase();
}

function logEmailQuota() {
  var emailQuotaRemaining = Email.mailer.getRemainingDailyQuota();
  Sheet.log('Remaining email quota: ' + emailQuotaRemaining);
}

function isStringOrNumber(value) {
  return isString(value) || isNumber(value);
}

function isString(value) {
  return isType('string', value);
}

function isNumber(value) {
  return isType('number', value);
}

function isType(type, value) {
  return typeof value === type;
}

function not(bool) {
  return !bool;
}

function isEmpty(value) {
  return value.trim().length === 0;
}

function isNotEmptyString(value) {
  return isString(value) && isNotEmpty(value);
}

function isNotEmpty(value) {
  return not(isEmpty(value));
}

function isMattermost(sheet) {
  var mattermostCell = Cell.from(HEADER.MATTERMOST, sheet.values);

  return (
    sheet.displayValue(sheet.headerRow, sheet.activeColumn) ===
      DISPLAY_VALUES.EMAIL &&
    (isEmpty(sheet.displayValues(sheet.activeRow, mattermostCell.column)) ||
      sheet.displayValues(sheet.activeRow, mattermostCell.column) ===
        DISPLAY_VALUES.NOID)
  );
}

function handleMattermost(sheet) {
  var token = getAuthToken();
  var emailCell = Cell.from(HEADER.EMAIL, sheet.values);
  var mattermostCell = Cell.from(HEADER.MATTERMOST, sheet.values);

  getUserName(
    sheet,
    emailCell.column,
    sheet.activeRow,
    mattermostCell.column,
    token,
    sheet.activeValue
  );
  var rowNum = sheet.headerRow + 1;
  while (sheet.displayValue(rowNum, sheet.activeColumn)) {
    if (isEmpty(sheet.displayValues(rowNum, mattermostCell.column))) {
      getUserName(
        sheet,
        emailCell.column,
        rowNum,
        mattermostCell.column,
        token
      );
    }
    rowNum++;
  }
}
