# [![CodingDojo](https://gitlab.com/nguyenmp/googlesheetsscript/raw/master/docs/images/LogoImage.png)](https://gitlab.com/nguyenmp/googlesheetsscript/)

This project is to allow easy managing of coding dojo students. 

It's written in GS(google script) and runs on top of a google sheet(online excel) from one of these templates [here](https://docs.google.com/spreadsheets/d/1rmMlt1wlOrkFCXv8hYxGdq81JwBI3wHoN_7G7iPNz6o/edit?usp=drive_web&ouid=116676133851635908442). 

## Initial Set Up of Attendance Sheet:
1. Go to your attendance sheet and click in your toolbar *__Tools__*>*__Script Editor__* in the top toolbar.
Note: For first timers that havent made a project, name it CodingDojoAutomation.
2. Copy and paste the code found in this github js file [here](https://gitlab.com/nguyenmp/googlesheetsscript/blob/master/main.js) into the Code.gs file of the script project.
It should look like this: ![Example of Google Script Page](https://gitlab.com/nguyenmp/googlesheetsscript/raw/master/docs/images/Example.png)
3. Need to allow scripts to run with certain permissions(first time only):
    1. Select the myOnEdit function from the dropdown.
    2. Execute by pressing the play button. 
    ![Function Bar](https://gitlab.com/nguyenmp/googlesheetsscript/raw/master/docs/images/FunctionBar.png)
    3. When the pop up appears, allow all permission(emails, access to sheets, third party software and api requests). 
4. On your Google App Script, go to the toolbar and access project properties by click *__File__*>*__Project Properties__* and add in 2 properties:
    1. MATTERMOSTEMAIL - Set this to be your Mattermost Email(must have system admin priviledge - contact your lead to have this added to your account)
    2. MATTERMOSTPASSWORD - Set this to be your Mattermost Password
Click Save. These will be used to retrieve/access names and in the future to add students to channels through the google script.
5. Setting onEdit triggers. Google Script restricts access to certain features on edit(making API request for example) so we need to set a onEdit trigger to allow this to happen. Access your triggers by clicking *__Edit__*>*__Current Project Triggers__* and a pop up will appear. Click to add a trigger and it should look like this:
![Triggers](https://gitlab.com/nguyenmp/googlesheetsscript/raw/master/docs/images/Triggers.png)
Click save.
6. If all done correctly, you should be able to edit a cell and a toast ui should appear like this:
![Toast UI](https://gitlab.com/nguyenmp/googlesheetsscript/raw/master/docs/images/ToastTest.png)
7. Edit the top part of the script to have your name, email and testing set to false so that it actually sends emails to students/community manager.
8. Complete. This initial set up only needs to be done once for an google sheet.

## Monthly To Do:
1. Copy the needed template to your attendance sheet. Like this: [Python Attendance](https://docs.google.com/spreadsheets/d/15rJn_-UWQWiD2ZE4YiP7MNovobd0F9SwIgM53pZOjjM/edit#gid=995415922)
Note: Need to do this every month as the template and script may change. Grabbing latest copy is best.
2. Adding the necessary discussion topics url. These can be found here:
C#: [Gitlab Link ](https://gitlab.com/nguyenmp/googlesheetsscript/blob/master/c%23discussionurls)
MEAN: [Gitlab Link ](https://gitlab.com/nguyenmp/googlesheetsscript/blob/master/meandiscussionurls)
Python: [Gitlab Link](https://gitlab.com/nguyenmp/googlesheetsscript/blob/master/pythondiscussionurls)
Webfun: [Gitlab Link](https://gitlab.com/nguyenmp/googlesheetsscript/blob/master/webfundiscussionurls) 
*Note:should already be in webfun template*
Adding these discussion topic urls into the script section located far right on the spread sheet. These should go in as data validation in the cell TO FILL underneath *__DT URL__*. Python 2 Example here: ![DataValidation](https://gitlab.com/nguyenmp/googlesheetsscript/raw/master/docs/images/DataValidation.png)
3. Fill in students in cells
4. Fill in date of first lecture(should auto populate existing date cells). Dates are __REQUIRED__ for discussion topics to work

## Features/Use
### Scripts
1. Scripts are used by chosing the script(Currently *Assignments*/*Retrieve Discussion Topics*) and changing the execute script value to *RUN*. 
    1. *__Retrieve Discussion Topics__* - Requires a chosen discussion topic url.  
    ![DiscussionTopicFeature](https://gitlab.com/nguyenmp/googlesheetsscript/raw/master/docs/images/DiscussionTopicFeature.gif)  
    The changes you see on the sheet is the script finding the names of users then updating the sheet in the appropriate place (based on row number and discussion topic column.)
    2. *__Assignments__* - Requires a chosen stack (python, webfundamentals, C#, or MEAN) in the *__STACK data__* slot. This will update the latest assignment column with data from the platform(platform does not update these well, so may need some manual looks). TODO: Further improvements can be a list of ALL completed assignments.  
    *__NOTE__* : You need to organize all students that are *NOT* "ON PACE", "PROBATION", or "FALLING BEHIND" to the bottom of your list. The script stops when it finds a status that is not one of these 3.
2. Emails are a pain to keep track of between rollbacks and drops. Handle all rollbacks and drops through the google sheet.
    1. Status updates : When a student status changes through the sheet, an email can be triggered depending on status change and creates a note in the __FIRST__ name cell. 
        * __Probation__ - Discussion topics are falling below necessary  
        ![StatusChangeEmail](https://gitlab.com/nguyenmp/googlesheetsscript/raw/master/docs/images/ProbationEmailExample.gif)
        * __Falling Behind__ - Assignments are falling behind  
        ![FallingBehind](https://gitlab.com/nguyenmp/googlesheetsscript/raw/master/docs/images/FallingBehindExample.gif)
        * __ROLLBACK__ - Student requested a rollback. HTML form will pop up to fill in necessary information. Email will send to Community Manager.  
        ![RollbackExample](https://gitlab.com/nguyenmp/googlesheetsscript/raw/master/docs/images/RollbackExample.gif)
        * __Dropped__ - Prompt will pop up to send reason for a drop. Email will send to community manager.
        ![DroppedExample](https://gitlab.com/nguyenmp/googlesheetsscript/raw/master/docs/images/DroppedExample.gif)
            
    2. Request next stack - Students that did not respond the first time. A change in NEXT STACK cell of a student to __REQUESTED__ value will send a email requesting what they would like for second stack. 

## Future Features:
    1. Change Discussion Topics - Changes the dates of discussion topics from google sheets using the urls and dates listed in the sheet.
    2. Update Assignment Script - Creates a list of ALL assignments done
    3. Exams - Scrapes exam data and auto populates exam column with dates and links to download
    4. Automation - Automatically runs discussion topics and assignment scripts on open of spread sheet